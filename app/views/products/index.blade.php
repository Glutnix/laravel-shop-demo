@extends('layouts.master')

@section('page-title') @parent
Products
@stop

@section('content')

<div class="container">

	<h2>Products</h2>
	@if(count($products) > 0)
		<ul>
		@foreach ($products as $product)
			<li>{{ link_to_route('products.show', $product->title . ' – $' . $product->price, $product->id) }}</li>
		@endforeach
		</ul>
	@else
		<p>No products.</p>
	@endif

</div> <!-- /container -->

@stop
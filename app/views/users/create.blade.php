@extends('layouts.master')

@section('page-title') @parent
Register
@stop

@section('content')

<div class="container">

	<div class="row">
		<div class="col-sm-4 col-sm-offset-4">

			<h2>Register New User</h2>
			{{ Form::open([
				'route' => 'users.store',
			]) }}

				<div class="form-group">
					{{ Form::label('email', 'Email') }}
					{{ Form::text('email', NULL, array(
						'placeholder' => 'email',
						'class' => 'form-control'
					)) }}
					@if ($errors->get('email'))
						<span class="help-block">
							<ul>
								@foreach( $errors->get('email') as $error )
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</span>
					@endif
				</div>

				<div class="form-group">
					{{ Form::label('password', 'Password') }}
					{{ Form::password('password', array(
						'class' => 'form-control'
					)) }}
					@if ($errors->get('password'))
						<span class="help-block">
							<ul>
								@foreach( $errors->get('password') as $error )
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</span>
					@endif
				</div>

				<div class="form-group">
					{{ Form::label('password_confirmation', 'Confirm') }}
					{{ Form::password('password_confirmation', array(
						'class' => 'form-control'
					)) }}
					@if ($errors->get('password'))
						<span class="help-block">
							<ul>
								@foreach( $errors->get('password_confirmation') as $error )
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</span>
					@endif
				</div>

				<div class="form-group">
					{{ Form::submit('Log In', array(
						'class' => 'btn btn-primary'
					)) }}
				</div>
			{{ Form::close() }}
			</div>
		</div>
	</div>
</div> <!-- /container -->

@stop
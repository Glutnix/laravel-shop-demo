<?php

class PaymentProcessor {
	

	function startPayment($amount, $returnUrl, $cancelUrl, $currency = "NZD")
	{
		$purchase = Omnipay::purchase([
			'amount'        => $amount,
			'currency'      => $currency,
			'returnUrl'     => $returnUrl,
			'cancelUrl'     => $cancelUrl,
			//'EmailAddress'  => 'brett.taylor@yoobee.ac.nz',
			//'transactionId' => 'banana', //uniqid(microtime()),
			//'description'   => 'lololol',
		])->send();
		
		if ( ! $purchase->isRedirect()) {
			throw new UnexpectedValueException("Payment gateway responded:\n ". $purchase->getMessage());
		}

		return $purchase->getRedirectUrl();
	}


	function isPaymentAccepted()
	{
		$confirm = Omnipay::completePurchase([
			'result' => Input::get('result')
		])->send();
		
		return $confirm->isSuccessful();
	}

}



